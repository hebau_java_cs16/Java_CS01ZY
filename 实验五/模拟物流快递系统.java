
public abstract class Transportation {
	private String num;// 编号
	private String model;// 型号
	private String head;// 负责人

	public Transportation() {

	}

	public Transportation(String num, String model, String head) {
		this.num = num;
		this.model = model;
		this.head = head;
	}
	public void setNum(String num) {
		this.num=num;
	}
	public String getNum() {
		return this.num;
	}
	public void setModel(String model) {
		this.model=model;
	}
	public String getModel() {
		return this.model;
	}
	public void setHead(String head) {
		this.head=head;
	}
	public String getHead() {
		return this.head;
	}
	public abstract void Transport();
}



public class JDTransportation extends Transportation{

	public void Transport() {
		System.out.println("京东快递运输中。。。");
		
	}

}



interface GPS {
public void showCoodinate();
}


public class Phone implements GPS {

	public void showCoodinate() {
		Phone b=new Phone();
		System.out.println("货物的当前坐标为103,485");
		
	}


}

public class SendTask {
private String num1;//运单号
private String weight;//货物的重量
public SendTask() {
	
}
public SendTask(String num1,String weight) {
	this.num1=num1;
	this.weight=weight;
}
public void setNum1(String num1) {
	this.num1=num1;
}
public String getNum1() {
	return this.num1;
}
public void setWeight(String weight) {
	this.weight=weight;
}
public String getWeight() {
	return this.weight;
}
//输出运输前的检查信息和快递单号
public void sendBefore(SendTask a) {
	System.out.println("订单开始处理，仓库开始验货");
	System.out.println("货物的重量"+a.getWeight());
	System.out.println("订单已发货");
	System.out.println("快递单号"+a.getNum1());
}
public void send(Transportation t,GPS tool) {
	t.setHead("小张");
	t.setNum("zh1002");
	t.setModel("长城");
	System.out.println("运货人"+t.getHead()+"正在驾驶编号为"+t.getNum()+"的"+t.getModel()+"发送货物");
	t.Transport();
	tool.showCoodinate();
}
public void sendAfter(Transportation t) {
	System.out.println("货物运输已完成");
	System.out.println("运货人"+t.getHead()+"正在驾驶编号为"+t.getNum()+"的"+t.getModel()+"已归还");
}


}
import java.util.Scanner;
public class Test {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		Transportation tr=new JDTransportation();
		GPS g=new Phone();
		SendTask s=new SendTask();
		System.out.println("请输入运单号");
		String str1=in.nextLine();
		s.setNum1(str1);
		System.out.println("请输入货物的重量");
		String str2=in.next();
		System.out.println();
		s.setWeight(str2);
		System.out.println();
		s.sendBefore(s);
		System.out.println();
		s.send(tr,g);
		System.out.println();
		s.sendAfter(tr);
	}

}
