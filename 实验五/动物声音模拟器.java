import 动物声音模拟器.Cat;
import 动物声音模拟器.Dog;
import 动物声音模拟器.Simulator;
import java.util.Scanner;
public class Test {

	public static void main(String[] args) {
		Simulator simulator=new Simulator();
		System.out.println("请输入要选择的动物：（狗为1，猫为2）");
		Scanner in=new Scanner(System.in);
		int s=in.nextInt();
		if(s==1) {
			simulator.playSound(new Dog());
		}
		if(s==2) {
			simulator.playSound(new Cat());
		}
		
	
	}

}


package 动物声音模拟器;

public interface Animal {
public abstract void cry();
public abstract String getAnimalName();
}


package 动物声音模拟器;

public class Simulator {
 public void playSound(Animal animal){
System.out.println("动物的种类是"+animal.getAnimalName());
animal.cry();
}
}


package 动物声音模拟器;

public class Dog implements Animal{

	public void cry() {
	System.out.println("汪汪叫");
		
	}

	public String getAnimalName() {
		return "狗";
		
		
	}

}

package 动物声音模拟器;
public class Cat implements Animal{

	public void cry() {
	System.out.println("喵喵叫");
		
	}

	public String getAnimalName() {
		return "猫";
		
		
	}

}