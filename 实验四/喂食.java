
public abstract class Animal {
public abstract void eat();
}

public class Lion extends Animal{
public void eat() {
	System.out.println("狮子吃饱了");
}
}

public class Monkey extends Animal {
	public void eat() {
		System.out.println("猴子吃饱了");
	}
}
public class Pegion extends Animal{
	public void eat() {
		System.out.println("鸽子吃饱了");
	}
}

public class Feeder {
	private Animal animal[];

	public Feeder() {

	}

	public void feederAnimal(Animal[] animal) {
		for (int i = 0; i < animal.length; i++) {
			animal[i].eat();
		}
	}

}
public class Test {

	public static void main(String[] args) {
		Feeder a = new Feeder();
		Lion[] lion = new Lion[1];
		Animal[] monkey = new Monkey[5];
		Animal[] pegion = new Pegion[10];
		System.out.println("请选择你要喂食的动物");
		Scanner in = new Scanner(System.in);
		String str = in.next();
		switch (str) {
		case "狮子": {
			lion[0] = new Lion();
			a.feederAnimal(lion);
			break;
		}
		case "猴子": {

			for (int i = 0; i < monkey.length; i++) {
				monkey[i] = new Monkey();
			}
			a.feederAnimal(monkey);

		}
		case "鸽子": {
			for (int i = 0; i < pegion.length; i++) {
				pegion[i] = new Pegion();
			}
			a.feederAnimal(pegion);

		}
		}
	}
}
