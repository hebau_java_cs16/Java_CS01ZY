package 部门;

public class Text {
	public static void main(String arsg[]) {
		Manage wor1 = new Manage();
		Zhiyuan wor2 = new Zhiyuan();
		wor1.setName("王门\t");
		wor1.setAge(30);
		wor1.setSex("\t男\t");
		wor1.setJob("部门经理\t");
		wor1.show();
		wor1.setYearsalary("120000\t");
		wor2.setDepartment("人力资源部\t");
		wor2.setMonthsalary("10000\t");
		System.out.println(wor1.getJob() + "\n" + wor1.getYearsalary() + "\t" + wor2.getDepartment() + "\t"
				+ wor2.getMonthsalary());
	}

}

package 部门;

public class Worker {
	private String name;
	private int age;
	private String sex;

	public Worker() {

	}

	public Worker(String name,int age, String sex) {
		this.name = name;
		this.age = age;
		this.sex = sex;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	public String getSex() {
		return this.sex;
	}
	public void setSex(String sex) {
		this.sex= sex;
	}
	public void show() {
		System.out.println(this.getName()+"\t"+this.getAge()+"\t"+this.getSex());
	}
}

package 部门;

public class Manage extends Worker{
	private String job;
	private String yearsalary;
	public String getJob() {
		return this.job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getYearsalary() {
		return this.yearsalary;
	}
	public void setYearsalary(String yearsalary) {
		this.yearsalary = yearsalary;
	}
}

package 部门;

public class Zhiyuan extends Worker{
private String department;
private String monthsalary;
public String getDepartment() {
	return this.department;
}
public void setDepartment(String department) {
	this.department = department;
}
public String getMonthsalary() {
	return this.monthsalary;
}
public void setMonthsalary(String monthsalary) {
	this.monthsalary = monthsalary;
}

}