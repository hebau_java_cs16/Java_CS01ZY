package score;

import java.util.Scanner;

public class Score {
	public static void main(String[] args) {
		int[][] score = new int[5][10];
		double[] ave1 = new double[5];
		int max1[] = new int[5];
		int min1[] = new int[5];
		Scanner in = new Scanner(System.in);
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 10; j++) {
				System.out.println("请第" + (j + 1) + "位评委给第" + (i + 1) + "位选手评分");
				score[i][j] = in.nextInt();
			}
		}
		max1 = max(score);
		min1 = min(score);
		ave1 = ave(score, max1, min1);
		System.out.println("每位选手平均分分别为：");
		for (int i = 0; i < 5; i++) {
			System.out.println(ave1[i]);
		}
	}

	public static int[] max(int[][] score) {
		int[] max2 = new int[5];
		for (int i = 0; i < score.length; i++) {
			max2[i] = score[i][0];
		}
		return max2;
	}

	public static int[] min(int[][] score) {
		int[] min2 = new int[5];
		for (int i = 0; i < score.length; i++) {
			min2[i] = score[i][4];
		}
		return min2;
	}

	public static double[] ave(int[][] score, int max[], int min[]) {
		double[] ave = new double[5];
		int[] sum = new int[5];
		for (int i = 0; i < score.length; i++) {
			for (int j = 0; j < score[i].length; j++) {
				sum[i] = sum[i] + score[i][j];
			}
			ave[i] = (sum[i] - max[i] - min[i]) / (score[i].length - 2);

		}
		return ave;
	}

}