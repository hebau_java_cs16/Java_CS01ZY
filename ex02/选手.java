package 选手;


import java.util.Arrays;
import java.util.Scanner;

public class Score {
	public static void main(String[] args) {
		double[] c;
		String name;
		Scanner in = new Scanner(System.in);
		System.out.println("请输入选手数：");
		int stu = in.nextInt();
		Player[] a = new Player[stu];// 创建选手类对象数组
		System.out.println("请输入评委数：");
		int num1 = in.nextInt();
		
		for (int i = 0; i < stu; i++) {
			System.out.println("请输入选手编号：");
			
			int s=in.nextInt();
			System.out.println("请输入选手姓名：");
			 name= in.next();
			 a[i]=new Player(name,s);//初始化a[i]
			System.out.println("请为" + (i+1) + "号选手打分：");
			Pingfen b=new Pingfen(num1);
            b.inputScore();
			c=b.getscore();  
	
			double totalscore=b.ave(c);
			System.out.println((i+1)+"号选手的最后得分是："+totalscore);
		   a[i].setscore(totalscore);
		}
		Arrays.sort(a);
		for(int i=0;i<stu;i++){
			System.out.println(a[i].toString());

	}
}
}


package 选手;



public class Player implements Comparable<Player> {
	private String name;
	private int num;
	private double score;
	public Player() {
	}
	public Player(String name, int num) {
		this.name = name;
        this.num=num;
       
	}
	public String getname() {
		return name;

	}

	public void setname(String name) {
		
		this.name = name;
	}
	public int getnum() {
		return this.num;

	}
	public void setnum(int num) {
		this.num=num;

	}
	public double getscore() {
		return score;
	}
	public void setscore(double score) {
		this.score=score;
	}


	public String toString(){
		return "姓名：\n"+this.getname()+"\t选手编号："+this.getnum()+"最后得分"+this.getscore();
	}

	
	public int compareTo(Player o) {
	if(this.score>o.score) {
		return -1;
	}
	else if(this.score<o.score) {
		return 1;
	}
	else {
		return 0;
	}
	
	}


}

package 选手;


import java.util.Scanner;
import java.util.Arrays;
//打分类
public class Pingfen {
	private double score[];// 评委打的分数
	private static int num1;// 评委人数
	Scanner in = new Scanner(System.in);

	public Pingfen() {
	}

	public Pingfen(int num1) {
       this.num1=num1;
	}


	public int getnum1() {
		return this.num1;
	}

	public void setnum1(int num1) {
		this.num1=num1;
	}

	public double[] getscore() {
		return this.score;
	}

	public void setscore(double[] score) {
		this.score=score;
	}
	public void  inputScore() {
		score=new double[num1];
		for(int i=0;i<num1;i++) {
			score[i]=in.nextDouble();
		}
	}
	
	public static double ave(double[] score) {
		double ave = 0;
		double sum = 0;
		double max1=score[0];
		double min1=score[0];
		Arrays.sort(score);
		for(int i=1;i<num1;i++) {
			if(max1<score[i]) {
				max1=score[i];
			}
			if(min1>score[i]) {
				min1=score[i];
			}	
			
		}
		System.out.println("去掉一个最高分"+max1);
		System.out.println("去掉一个最低分"+min1);
		for (int i = 0; i < num1; i++) {
				sum = sum + score[i];
			}
			int i=0;
			ave = (sum - max1 - min1) / (num1- 2);
			return ave;
	}
	
}


