import java.util.Scanner;

public class Calendar {
	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("请输入年份和月份");

		int year = in.nextInt();
		int month = in.nextInt();
		if (isLeap(year) == true) {
			System.out.println(year + "年是闰年");
		} else {
			System.out.println(year + "年不是闰年");
		}
		printCalender(year, month);
	}

	// 判断是否为闰年
	public static boolean isLeap(int year) {

		if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
			return true;
		}

		else {
			return false;
		}
	}

	// 判断某年某月有多少天
	public static int days(int year, int month) {
		int days = 0;
		boolean a;
		a = isLeap(year);
		if(a==true){
			if(month==1||month==3||month==5||month==7||month==8||month==10||month==12){
				days=31;
			}
			else if(month==4||month==6||month==9||month==11){
				days=30;
			}
			else{
				days=29;
			}
		}
			else{
				if(month==1||month==3||month==5||month==7||month==8||month==10||month==12){
					days=31;
				}
				else if(month==4||month==6||month==9||month==11){
					days=30;
				}
				else{
					days=28;
				}
			}
			return days;
	}
	// 计算某年某月距离之前距离1900年1月1日的天数
	public static int totalDays(int year, int month) {
		int days1 = 0, days2 = 0, days3 = 0, j;
		for (int i = 1900; i < year; i++) {
			if (isLeap(i)) {
				days1 += 366;
			} else {
				days1 += 365;
			}
		}
		for (j = 1; j < month; j++) {
			days2 += days(year, j);
		}
		days3 = days1 + days2;
		return days3;
	}

	public static void printCalender(int year, int month) {
		int s1, s2, s3, s, t;
		s1 = days(year, month);
		System.out.println("这个月的天数为:"+s1);
		s2 = totalDays(year, month);
		System.out.println("距1900年1月1日天数为:"+s2);
		s3 = (1 + s2 % 7);
		System.out.println("月初为星期:"+s3);
		System.out.println("星期一\t星期二\t星期三\t星期四\t星期五\t星期六\t星期日");
		for (s = 1; s < s3; s++) {
			System.out.printf("\t");
		}
		for (int i = 1; i <= s1; i++) {
			System.out.printf("%2d\t", i);
			if ((i + s3 - 1) % 7 == 0) {
				System.out.printf("\n");
			}
		}

	}

}